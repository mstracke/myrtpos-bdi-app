package com.rtpos.myrtpos.myrtpos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.view.MenuInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebViewClient;
import android.webkit.WebResourceRequest;

public class BDIActivity extends AppCompatActivity {


    private WebView webView;
    private boolean bEnableZoom = false;
    private boolean bBrowserLinkMode = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bdi);

        webView = (WebView)findViewById(R.id.webMyRTPOS);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setDisplayZoomControls(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.loadUrl("http://www.myrtpos.com/newbdi/index.fwx");
        webView.setWebViewClient(new WebViewClient());



    }

    @Override
    public boolean onPrepareOptionsMenu (Menu menu){
        menu.findItem(R.id.mnu_back).setEnabled(webView.canGoBack());
        menu.findItem(R.id.mnu_fwd).setEnabled(webView.canGoForward());
        menu.findItem(R.id.mnu_browser).setChecked(bBrowserLinkMode);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.mnu_back:
                goBack();
                return true;
            case R.id.mnu_fwd:
                goForward();
                return true;
            case R.id.mnu_reload:
                refresh();
                return true;
            case R.id.mnu_browser:
                bBrowserLinkMode = !bBrowserLinkMode;
                if(!bBrowserLinkMode)
                    webView.setWebViewClient(new WebViewClient());
                else
                    webView.setWebViewClient(null);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void goBack(){

        if(webView.canGoBack())
            webView.goBack();
    }

    private void goForward(){
        if(webView.canGoForward())
            webView.goForward();
    }

    private void refresh(){
        webView.reload();
    }



}
